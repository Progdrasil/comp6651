from tests.conftest import from_file_pairs
from typing import List
import pytest
from p1.hotel import Sizes, algorithm, program
from hypothesis import given, strategies as st
from os import path


@given(
    L=st.integers(1, int(10e8)),
    F=st.integers(1, 5000),
    M=st.integers(1, 5000),
    D=st.integers(1, 5000),
    C=st.integers(1, 5000),
)
def test_parsing_initial_line(L: int, F: int, M: int, D: int, C: int):
    sizes = Sizes(f"{L} {F} {M} {D} {C}")

    assert sizes.L == L
    assert sizes.F == F
    assert sizes.M == M
    assert sizes.D == D
    assert sizes.C == C


def test_parsing_exits():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        Sizes(f"0 0 0 0 0")
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


# Worst case scenario
@pytest.mark.timeout(30)
def test_algorithm_speed(
    L: int = int(10e8),
    prices: List[List[int]] = [
        list(range(0, 5000)),
        list(range(0, 5000)),
        list(range(0, 5000)),
        list(range(0, 5000)),
    ],
):
    algorithm(L, prices)


@pytest.mark.parametrize("inp", [1, "meals"])
def test_example(inp: str):

    with pytest.raises(SystemExit) as escape:
        from_file_pairs("p1", inp, lambda s: int(s.strip()), program, 0)

    assert escape.type == SystemExit
    assert escape.value.code == 0
