from tests.conftest import from_file_pairs
from p2.recordings import entrypoint
import pytest


@pytest.mark.parametrize(
    "set",
    [
        1,
        2,
        3,
        4,
        "00-lukas",
        "1",
        "2",
        "3",
        "04-dontknow",
        "4",
        "05-equal",
        "06-michal",
        "07-onelong",
        "08-properlong.0",
        "09-properlong.1",
        "10-properlong.2",
        "11-properlong.3",
        "12-properlong.4",
        "13-properlong.5",
        "14-uniform.0",
        "15-uniform.1",
        "16-uniform.2",
        "17-uniform.3",
        "18-uniform.4",
        "19-uniform.5",
        "20-uniformlong.0",
        "21-uniformlong.1",
        "22-uniint.0",
        "23-uniint.1",
        "24-uniint.2",
        "25-uniint.3",
        "26-uniint.4",
        "27-uniint.5",
        "28-uniintlong.0",
        "29-uniintlong.1",
        "30-simple",
    ],
)
def test_example_inputs_conf(set: str):
    from_file_pairs("p2", set, lambda s: int(s.strip()), entrypoint)