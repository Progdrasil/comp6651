import pytest
from .conftest import from_file_pairs
from p7.trading import entrypoint


@pytest.mark.parametrize("slug", ["sample", "fb"])
def test_files(slug: str):
    from_file_pairs(
        "p7", slug, lambda s: s.strip() == "TRUE", entrypoint, is_generator=True
    )
