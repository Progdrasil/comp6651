from tests.conftest import from_file_pairs
import pytest
from p4.gift_basket import entrypoint


@pytest.mark.parametrize("slug", ["example", "i"])
def test_files(slug: str):
    from_file_pairs(
        "p4",
        slug,
        lambda s: int(s.strip().split()[1]),
        entrypoint,
        preprocess=lambda f: f.readline(),
    )
