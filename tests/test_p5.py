import pytest
from tests.conftest import from_file_pairs
from p5.tunnel import entrypoint


@pytest.mark.parametrize("slug", ["example", "fb", "A.0", "A.1-2", "A.4", "A.5"])
def test_files(slug: str):
    from_file_pairs("p5", slug, lambda s: int(s.strip()), entrypoint, is_generator=True)