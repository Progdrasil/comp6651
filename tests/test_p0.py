from p0.tournament import algorithm
from os import path
import pytest


@pytest.mark.parametrize("file,result", [("inp1.txt", 5), ("inp2.txt", 34)])
def test_read_from_file(file: str, result: int):
    file = path.join("p0", file)
    with open(file) as f:
        n = int(f.readline().strip())
        x = algorithm(n, f)
    print(x)
    assert x == result
