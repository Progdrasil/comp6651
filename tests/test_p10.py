import pytest
from .conftest import from_file_pairs
from p10.lzw import entrypoint


@pytest.mark.parametrize("slug", ["sample", "aux_test", "judge"])
def test_files(slug: str):
    from_file_pairs(
        "p10", slug, lambda s: s.strip().split()[2], entrypoint, is_generator=True
    )
