import pytest
from p9.game import entrypoint, min_gcd
from .conftest import from_file_pairs


@pytest.mark.parametrize("slug", ["sample", "fb1", "fb2", "fb3"])
def test_files(slug: str):
    from_file_pairs("p9", slug, lambda s: int(s.strip()), entrypoint, is_generator=True)


@pytest.mark.timeout(60)
def test_worst_possible():
    for _ in range(1000):
        min_gcd(list(range(2, 102)), 100)