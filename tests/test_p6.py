import pytest
from tests.conftest import from_file_pairs
from p6.baricades import entrypoint
from p6.sol import main

files = ["example", "sample", "1"]


@pytest.mark.parametrize("slug", files)
def test_files(slug: str):
    from_file_pairs("p6", slug, lambda s: int(s.strip()), entrypoint, is_generator=True)


@pytest.mark.parametrize("slug", files)
def test_solution(slug: str):
    from_file_pairs("p6", slug, lambda s: int(s.strip()), main, is_generator=True)
