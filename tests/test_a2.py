from a2.q2 import count_large_reversal, find_large_reversal


A = [7, 2, 40, 13, 4, 1, 6, 12]


def test_reversal():

    assert find_large_reversal(A, len(A)) == 16


def test_count_reversal():

    assert count_large_reversal(A, 0, len(A) - 1) == 4
