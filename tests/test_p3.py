from typing import Optional
from tests.conftest import from_file_pairs
import pytest

from p3.groupings import entrypoint


def extract_second(i: str) -> Optional[int]:
    l = i.strip().split()
    return int(l[1]) if len(l) > 1 else None


@pytest.mark.parametrize("slug", ["sample", "fb", "chop_clean"])
def test_with_samples(slug: str):
    from_file_pairs("p3", slug, extract_second, entrypoint)
