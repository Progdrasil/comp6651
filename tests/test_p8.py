import pytest
from p8.puzzle import entrypoint
from .conftest import from_file_pairs


@pytest.mark.parametrize("slug", ["sample", "judge"])
def test_files(slug: str):
    from_file_pairs("p8", slug, lambda s: s.strip(), entrypoint, is_generator=True)