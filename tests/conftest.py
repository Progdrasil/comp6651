from typing import (
    Any,
    Callable,
    Generator,
    Optional,
    TextIO,
    TypeVar,
    List,
    Union,
    cast,
)
from os import path

T = TypeVar("T")


def from_file_pairs(
    assignment: str,
    slug: str,
    exp_f: Callable[[str], T],
    entrypoint: Callable[[TextIO], Union[T, Generator[T, None, None]]],
    exp_tail: Optional[T] = None,
    preprocess: Optional[Callable[[TextIO], Any]] = None,
    is_generator: bool = False,
):
    dir = path.join(path.abspath(assignment), "data")
    inp = path.join(dir, f"{slug}.in")
    out = path.join(dir, f"{slug}.out")

    with open(out, "r") as o:
        expected: List[T] = []
        for i in o.readlines():
            expected.append(exp_f(i))

    if exp_tail is not None:
        expected.append(exp_tail)

    with open(inp, "r") as f:
        if preprocess is not None:
            preprocess(f)

        if is_generator:
            output = cast(Generator[T, None, None], entrypoint(f))
        else:
            output = (cast(T, entrypoint(f)) for _ in range(0, len(expected)))

        for i, out in enumerate(output):
            assert expected[i] == out