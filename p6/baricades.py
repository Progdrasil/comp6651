from collections import defaultdict, deque
import sys
from typing import DefaultDict, Dict, Generator, NamedTuple, TextIO

Coord = NamedTuple("Coord", [("x", int), ("y", int)])
Edge = NamedTuple("Edge", [("weight", int), ("point", Coord)])
Flow = DefaultDict[Coord, DefaultDict[Coord, int]]


def above(src: Coord) -> Coord:
    return Coord(src.x, src.y - 1)


def prev(src: Coord) -> Coord:
    return Coord(src.x - 1, src.y)


class AdjList:
    def __init__(self, type: str) -> None:
        self.type = type
        self.edges: Dict[Coord, int] = {}
        pass

    def isAccessible(self) -> bool:
        return self.type in ".D"

    def __getitem__(self, index: Coord) -> int:
        if index not in self.edges:
            self.edges[index] = 0
        return self.edges[index]

    def __setitem__(self, index: Coord, value: int):
        self.edges[index] = value


class Graph:
    def __init__(self, stream: TextIO) -> None:
        (self.rows, self.cols) = map(int, stream.readline().strip().split())
        self.source = Coord(-1, -1)
        self.sink = Coord(self.rows + 1, self.cols + 1)
        # self.size = rows * cols + 2  # +2 for source and sink
        self.adj_list: Dict[Coord, AdjList] = {
            self.source: AdjList("S"),
            self.sink: AdjList("D"),
        }

        for y in range(self.rows):
            row = stream.readline().strip()
            for x in range(self.cols):
                point = Coord(x, y)
                self.add_vertex(point, row[x])

    def add_vertex(self, point: Coord, value: str):
        def edge(u: Coord, v: Coord, capacity: int = 1):
            if u not in self.adj_list:
                self.adj_list[u] = AdjList(value)
            self.adj_list[u][v] += capacity
            self.adj_list[v][u] += capacity

        if value == "X":
            return

        abv = above(point)
        previous = prev(point)

        if value == "D":
            edge(point, self.sink, 4)
        if point.y == 0:
            edge(point, self.source)
        elif abv in self.adj_list and self.adj_list[abv].isAccessible:
            edge(point, abv)

        if point.x == 0:
            edge(point, self.source)
        elif previous in self.adj_list and self.adj_list[previous].isAccessible:
            edge(point, previous)

        if point.x == self.cols - 1:
            edge(point, self.source)
        if point.y == self.rows - 1:
            edge(point, self.source)

    def __getitem__(self, index: Coord) -> AdjList:
        return self.adj_list[index]


def max_flow(G: Graph) -> int:

    F: Flow = defaultdict(lambda: defaultdict(int))
    while True:
        parents = {G.source: G.source}
        path_capacity = {G.source: sys.maxsize}

        queue = deque([G.source])
        while queue:
            u = queue.popleft()
            for v, cap_u_v in G[u].edges.items():
                if cap_u_v - F[u][v] > 0 and v not in parents:
                    parents[v] = u
                    path_capacity[v] = min(path_capacity[u], cap_u_v - F[u][v])
                    if v != G.sink:
                        queue.append(v)
                    else:
                        while parents[v] != v:
                            u = parents[v]
                            F[u][v] += path_capacity[G.sink]
                            F[v][u] -= path_capacity[G.sink]
                            v = u
                        break
        if G.sink not in parents:
            return sum(F[G.source].values())


def entrypoint(stream: TextIO) -> Generator[int, None, None]:
    N = int(stream.readline().strip())
    for _ in range(N):
        # Build graph
        G = Graph(stream)
        yield max_flow(G)


if __name__ == "__main__":
    for res in entrypoint(sys.stdin):
        print(res)