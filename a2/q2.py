from typing import List
import sys


def find_large_reversal(A: List[int], n: int) -> int:
    A.sort()

    return large_reversal(A, 0, n)


def large_reversal(A: List[int], l: int, r: int) -> int:
    count = 0
    if l < r:
        m = (r + l) // 2
        count += large_reversal(A, l, m)
        count += large_reversal(A, m + 1, r)

        L = A[l:m]
        L.append(sys.maxsize)
        R = A[m:r]
        R.append(0)
        i = 0
        j = 0
        for k in range(l, r):
            if R[j] > 3 * L[i]:
                count += j + 1
                i += 1
            elif j == r - 1:
                i += 1
            else:
                j += 1

    return count


def count_large_reversal(A: List[int], l: int, r: int) -> int:
    count = 0
    for i in range(l, r):
        for j in range(i + 1, r):
            if A[i] > 3 * A[j]:
                count += 1

    return count


# [7, 2, 40, 13, 4, 1, 6, 12]
