from collections import defaultdict, deque
from typing import DefaultDict, Dict, Generator, List, Optional, TextIO, Tuple
from sys import stdin

movement_vectors = {
    "F": (0, 0, -1),
    "B": (0, 0, 1),
    "L": (0, 1, 0),
    "R": (0, -1, 0),
    "U": (1, 0, 0),
    "D": (-1, 0, 0),
}

movements = {
    "F": ["R", "L", "U", "D"],
    "B": ["R", "L", "U", "D"],
    "L": ["F", "B", "U", "D"],
    "R": ["F", "B", "U", "D"],
    "U": ["F", "B", "R", "L"],
    "D": ["F", "B", "R", "L"],
}

inverse = {"F": "B", "B": "F", "R": "L", "L": "R", "U": "D", "D": "U"}


Point = Tuple[int, int, int]

map_order = "FRBLUD"
map_start_mask = {
    "F": (1, 1, 0),
    "R": (1, 0, 0),
    "B": (1, 0, 1),
    "L": (1, 1, 1),
    "U": (1, 1, 1),
    "D": (0, 1, 0),
}
map_directions = {
    "F": (-1, -1, 0),
    "R": (-1, 0, 1),
    "B": (-1, 1, 0),
    "L": (-1, 0, -1),
    "U": (0, -1, -1),
    "D": (0, -1, 1),
}


class Map:
    def __init__(self, n: int):
        self.n = n
        self.map = [[[True for z in range(n)] for y in range(n)] for x in range(n)]
        self.map_start: Dict[str, List[int]] = {
            k: list(map(lambda i: i * (n - 1), v)) for (k, v) in map_start_mask.items()
        }
        self.face_mask = {
            "F": (1, 1, 0),
            "B": (1, 1, n - 1),
            "R": (1, 0, 1),
            "L": (1, n - 1, 1),
            "U": (n - 1, 1, 1),
            "D": (0, 1, 1),
        }

    def read_map(self, stream: TextIO):
        for face in map_order:
            for row in range(self.n):
                data = stream.readline().strip()
                for col in range(self.n):
                    x, y, z = self.position(face, row, col)
                    self.map[x][y][z] = data[col] == " "

    def position(self, face: str, row: int, col: int) -> List[int]:
        mod = row if face not in "UD" else col
        coords = [0, 0, 0]
        for i in range(3):
            coords[i] = self.map_start[face][i] + mod * map_directions[face][i]
            if map_directions[face][i] != 0:
                mod = col if face not in "UD" else row

        return coords

    def move(self, move: str, p: Point) -> Optional[Point]:
        m = movement_vectors[move]
        x = p[0] + m[0]
        y = p[1] + m[1]
        z = p[2] + m[2]
        return (x, y, z) if x >= 0 and y >= 0 and z >= 0 else None

    def is_accessible(self, point: Point) -> bool:
        access: List[bool] = []
        for face in map_order:
            mask = self.face_mask[face]
            x, y, z = map(
                lambda i: point[i] * mask[i] if mask[i] == 1 else mask[i], range(3)
            )
            access.append(self.map[x][y][z])
        return all(access)


class Graph:
    def __init__(self, map: Map):
        self.n = map.n
        self.adj: DefaultDict[Point, Dict[Point, str]] = defaultdict(dict)
        self.dst = (self.n - 2, self.n - 2, self.n - 2)
        self.none = (-1, -1, -1)
        self.pred: Dict[Point, Point] = defaultdict(lambda: self.none)
        self.build_from_map(map)

    def build_from_map(self, map: Map):
        start = (1, 1, 1)
        queue: deque[Point] = deque([start])
        visited: DefaultDict[Point, bool] = defaultdict(lambda: False)
        visited[start] = True

        while queue:
            pos = queue.popleft()
            for face in map_order:
                for move in movements[face]:
                    neighbor = map.move(move, pos)

                    if neighbor is not None and map.is_accessible(neighbor):
                        self.add_edge(pos, neighbor, move)
                        if not visited[neighbor]:
                            visited[neighbor] = True
                            self.pred[neighbor] = pos
                            queue.append(neighbor)

                            if neighbor == self.dst:
                                return

    def add_edge(self, u: Point, v: Point, move: str):
        self.adj[u][v] = move
        self.adj[v][u] = inverse[move]

    def path(self) -> str:
        pred = self.pred
        moves = ""
        prev = self.dst

        while pred[prev] != self.none:
            moves += self.adj[pred[prev]][prev]
            prev = pred[prev]

        return moves[::-1]


def entrypoint(stream: TextIO) -> Generator[str, None, None]:
    n = int(stream.readline().strip())
    while n > 0:
        map = Map(n)
        map.read_map(stream)
        graph = Graph(map)

        yield graph.path()

        n = int(stream.readline().strip())
    return


if __name__ == "__main__":
    for path in entrypoint(stdin):
        print(path)