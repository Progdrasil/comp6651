#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <queue>
//#include <map>
using namespace std;

char dirname[6] = {
    'F',
    'B',
    'L',
    'R',
    'U',
    'D'
};

int updatedir[6][3] = {
    {-1, 0, 0 },
    {+1, 0, 0 },
    {0, +1, 0},
    {0, -1, 0},
    {0, 0, +1},
    {0, 0, -1}
};

int cube[32][32][32];

int main() {
    while (true) {
        string nstr;
        getline(cin, nstr);
        istringstream iss(nstr);
        int n;
        iss >> n;
        if(n == 0) break;
        
        vector<string> faces[6];
        for(int i = 0; i < 6; i++) {
            for(int j = 0; j < n; j++){
                string tempstr;
                getline(cin, tempstr);
                faces[i].push_back(tempstr);
            }
        }
        
        /*for(int i = 0; i < 6; i++) {
            for(int j = 0; j < n; j++) {
                cout << faces[i][j] << endl;
            }
        }*/
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                for(int k = 0; k < n; k++) {
                    cube[i][j][k] = -2; // unvisited
                }
            }
        }
        
        vector<int> start(3, 1);
        cube[1][1][1] = -1; // mark the start
        queue<vector<int> > q;
        q.push(start);
        while(!q.empty()) {
            vector<int> cur = q.front();
            q.pop();
            if(cur[0] == n-2 && cur[1] == n-2 && cur[2] == n-2) break;
            for(int i = 0; i < 6; i++) {
                vector<int> next(3, 0);
                for(int j = 0; j < 3; j++) {
                    next[j] = cur[j] + updatedir[i][j];
                }
                bool isok = true;
                
                int ni = next[0], nj = next[1], nk = next[2];
                if(ni <= 0 || ni >= n-1 || nj <= 0 || nj >= n-1
                   || nk <= 0 || nk >= n-1 || cube[ni][nj][nk] != -2) continue;
                if(faces[0][n-1-nk][n-1-nj]=='X') isok = false;
                if(faces[1][n-1-nk][ni]=='X') isok = false;
                if(faces[2][n-1-nk][nj]=='X') isok = false;
                if(faces[3][n-1-nk][n-1-ni]=='X') isok = false;
                if(faces[4][n-1-ni][n-1-nj]=='X') isok = false;
                if(faces[5][ni][n-1-nj]=='X') isok = false;
                
                if(isok) {
                    cube[next[0]][next[1]][next[2]] = i;
                    q.push(next);
                }
                
            }
        }
        string ans = "";
        int curi = n-2, curj = n-2, curk = n-2;
        int curval = cube[n-2][n-2][n-2];
        while ( curval != -1 ) {
            ans += dirname[curval];
            curi -= updatedir[curval][0];
            curj -= updatedir[curval][1];
            curk -= updatedir[curval][2];
            curval = cube[curi][curj][curk];
        }
        reverse(ans.begin(), ans.end());
        cout << ans << endl;
    }
    return 0;
}
