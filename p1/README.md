# Overlook Hotel

**Due Date:** 2021-01-31 23:55
**TimeLimit:** 5 sec (C/C++), 10 sec (Java), 30 sec (python)

## Description

Jack Torrance is taking care of Overlook Hotel with his family.
Unfortunately, lock down measures due to COVID-19 were announced and Jack got stuck in the hotel.
Overlook Hotel is very isolated and there is not much to do.
Jack decides that in order to keep his sanity he is going to order a different meal each day (the novelty of meals brings joy to the other wise monotonous life).
The first day a meal repeats, Jack will go insane and will try to harm his family.
Jack’s meal always consists of four parts:

1. a first dish,
2. a main dish,
3. a drink,
4. and a candy.

Two meals are different if they differ in at least one component.
Fortunately, food delivery services still work and are able to reach Overlook Hotel.
Jack has access to the menu on his phone where he has a selection of $`F`$ different first dishes, $`M`$ different main dishes, $`D`$ different drinks, and $`C`$ different candies (of course the menu specifies prices as well).
Jack is very careful about his expenses and insists that each meal does not exceed a fixed limit $`L`$.
Your goal is, given the limit $`L`$ and the menu, to determine how many days it will take until Jack is forced to repeat his meal and goes insane as a result of this.
This information is crucial to help save Jack’s family, so you need to design a fast algorithm to compute the number of different meal combinations with the price of each meal not exceeding $`L`$.
Jack’s family depends on you!

## Input

There are multiple test cases in a single input.
Each test case starts with a line containing five integers $`L, F, M, D, C`$ ($`1 \leq L \leq 10^8, 1 \leq F, M, D, C \leq 5000`$) representing the meal price limit, the number of first dishes, the number of main dishes, the number of drinks, and the number of candies in the menu, respectively.
Each of the next four lines contains a list of prices.
The first line contains the first dishes price list, the second line contains the main dishes price list, the third line contains the drinks price list, and the fourth line contains the candies price list.
All elements in the lists are positive integers of value at most $`10^8`$.
There is one empty line after each test case.
The input is terminated by a line with five zeros.

## Output

For each test case print on a separate line the number of different meals which can be created from the menu and have price not exceeding $`L`$.
Please, note that the value of the solution might not fit into 32-bit integers.

## Example

**input**
```
11 3 1 1 1
4 5 6
3
2
1

10 4 5 4 2
3 2 5 7
1 1 8 4 2
3 5 2 1
2 3

0 0 0 0 0
```
**output**
```
2
48
```

_Hint: brute force solution $`O(n^4)`$ is too slow, where $`n=F+M+D+C`$._
_Solution of time $`O(n^3)`$ might also be too slow. Intended solution has runtime $`O(n^2\log n) `$ and uses binary search._