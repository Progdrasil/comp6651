from typing import List, TextIO
from sys import stdin


class Sizes:
    def __init__(self, line: str):
        split = line.strip().split()
        (self.L, self.F, self.M, self.D, self.C) = map(int, split)
        if self.L == 0 and self.F == 0 and self.M == 0 and self.D == 0 and self.C == 0:
            exit(0)


class Problem:
    def __init__(self, stream: TextIO):
        sz = Sizes(stream.readline())
        self.sizes = sz
        pc = [
            prices(stream.readline()),  # F
            prices(stream.readline()),  # M
            prices(stream.readline()),  # D
            prices(stream.readline()),  # C
        ]
        # pc.sort(key=len)
        # for i in range(0, 4):
        #     pc[i] = pc[i][: search(self.sizes.L, pc[i], 0, len(pc[i]))]

        self.prices: List[List[int]] = pc


def prices(line: str) -> List[int]:
    split = line.strip().split()
    return list(sorted(map(int, split)))


def program(stream: TextIO) -> int:
    prob = Problem(stream)
    stream.readline()  # drop empty line

    return algorithm(prob.sizes.L, prob.prices)


def recursive_search(limit: int, arr: List[int], l: int, r: int) -> int:
    if r > l:
        mid = (r + l) // 2
        if limit < arr[mid]:
            return recursive_search(limit, arr, l, mid)
        else:
            return recursive_search(limit, arr, mid + 1, r)
    else:
        return l


def search(limit: int, arr: List[int], l: int, r: int) -> int:
    if limit >= arr[r - 1]:
        return r
    elif limit < arr[l]:
        return l
    while r > l:
        mid = (r + l) // 2
        if limit < arr[mid]:
            r = mid
        else:
            l = mid + 1
    return l


def algorithm(limit: int, P: List[List[int]]) -> int:
    left = []
    right = []
    for f in P[0]:
        for m in P[1]:
            left.append(f + m)

    for d in P[2]:
        for c in P[3]:
            right.append(d + c)

    left.sort()
    right.sort()

    count = 0
    for i in range(0, len(left)):
        count += search(limit - left[i], right, 0, len(right))

    return count


if __name__ == "__main__":
    while True:
        print(program(stdin))
