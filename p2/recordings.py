from typing import NamedTuple, TextIO
from sys import stdin, maxsize


class Show(NamedTuple):
    s: int
    f: int


def entrypoint(stream: TextIO) -> int:
    (n, k) = map(int, stream.readline().strip().split())

    shows = []
    for _ in range(0, n):
        shows.append(Show(*map(int, stream.readline().strip().split())))

    shows.sort(key=lambda s: s.f)

    # set end times of all tapes to 0
    tapes = [0] * k

    count = 0
    for show in shows:
        j = None
        for i in range(0, k):
            if tapes[i] <= show.s:
                if j is None or tapes[i] > tapes[j]:
                    j = i
        if j is not None:
            count += 1
            tapes[j] = show.f

    return count


if __name__ == "__main__":
    print(entrypoint(stdin))