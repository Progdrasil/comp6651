from typing import Generator, TextIO, Dict
from sys import stdin


class Node:
    def __init__(self, v: str):
        self.parent = v
        self.size = Height()
        self.rank = 0


class Height:
    def __init__(self):
        self.value: int = 1


def switch_heights(u: Node, v: Node):
    u.size.value += v.size.value
    v.size = u.size  # change pointer


class Graph:
    def __init__(self):
        self.disjoint_set: Dict[str, Node] = {}

    def add_vertice(self, v: str):
        if self.disjoint_set.get(v) is None:
            self.disjoint_set[v] = Node(v)

    # find with path compression
    def find(self, v: str) -> Node:
        u = self.disjoint_set[v]
        if u.parent != v:
            root = self.find(u.parent)
            if root.size != u.size:
                u.size = root.size
            u.parent = root.parent
            return root
        return u

    # Union by rank
    def union(self, u: str, v: str) -> int:
        x = self.find(u)
        y = self.find(v)

        if x.rank < y.rank:
            x.parent = y.parent
            switch_heights(x, y)
        elif x.rank > y.rank:
            y.parent = x.parent
            switch_heights(y, x)
        else:
            x.parent = y.parent
            x.rank += 1
            switch_heights(x, y)

        return x.size.value

    def kruskal(self, u: str, v: str) -> int:
        x = self.find(u)
        y = self.find(v)

        if x.parent != y.parent:
            self.union(x.parent, y.parent)

        return x.size.value


def entrypoint(stream: TextIO) -> Generator[int, None, None]:
    test_cases = int(stream.readline().strip())
    for _ in range(0, test_cases):
        bridges = int(stream.readline().strip())
        G = Graph()
        for _ in range(0, bridges):
            (u, v) = stream.readline().strip().split()
            G.add_vertice(u)
            G.add_vertice(v)
            count = G.kruskal(u, v)
            yield count


if __name__ == "__main__":
    for out in entrypoint(stdin):
        print(out)