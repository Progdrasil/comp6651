#include <iostream>
#include <map>
#include <string>
using namespace std;

#define MAXN 300000

int rep[MAXN];
int mycount[MAXN];

int lookup(int node) {
    if(rep[node]==node) return node;
    else return rep[node] = lookup(rep[node]);
}

int merge(int rep1, int rep2) {
    if(mycount[rep1] > mycount[rep2]) {
        rep[rep2] = rep1;
        mycount[rep1] += mycount[rep2];
        return mycount[rep1];
    } else {
        rep[rep1] = rep2;
        mycount[rep2] += mycount[rep1];
        return mycount[rep2];
    }
}

int main() {
    int numcase;
    cin >> numcase;
    while(numcase > 0) {
        int n;
        cin >> n;
        for(int i = 0; i < MAXN; i++) {
            rep[i] = i;
            mycount[i] = 1;
        }
        map<string, int> nametoint;
        int numseen = 0;
        for(int i = 0; i < n; i++) {
            string a1, a2;
            cin >> a1 >> a2;
            if(nametoint.find(a1) == nametoint.end()) {
                nametoint[a1]=numseen++;
            }
            if(nametoint.find(a2)==nametoint.end()) {
                nametoint[a2]=numseen++;
            }
            int rep1 = lookup(nametoint[a1]);
            int rep2 = lookup(nametoint[a2]);
            //cout << rep1 << " " << rep2 << endl;
            if(rep1 != rep2) {
                cout << merge(rep1, rep2) << endl;
            } else {
                cout << mycount[rep1] << endl;
            }
        }
        numcase--;
    }
    return 0;
}
