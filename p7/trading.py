from collections import defaultdict, deque
from sys import stdin
from typing import DefaultDict, Generator, List, TextIO, Tuple

INFTY = float("Inf")

Edge = Tuple[int, int, float]


class Graph:
    def __init__(self, T: int, R: int, stream: TextIO):
        self.R = R
        self.edges: List[Edge] = []
        self.graph: DefaultDict[int, List[int]] = defaultdict(list)

        for _ in range(T):
            line = stream.readline().strip().split()
            want = int(line[0]) - 1
            offer = int(line[1]) - 1
            amount = float(line[2])
            self.edges.append((want, offer, -amount))
            self.graph[want].append(offer)

    def path_exists(self, src: int, dest: int) -> bool:
        src -= 1
        dest -= 1
        visited = [False for _ in range(self.R)]

        queue = deque([src])
        visited[src] = True

        while queue:
            n = queue.popleft()

            if n == dest:
                return True

            for i in self.graph[n]:
                if not visited[i]:
                    queue.append(i)
                    visited[i] = True

        return False

    def inverse_bellman_ford(self, src: int) -> bool:
        dist = [INFTY if src != i else 0.0 for i in range(1, self.R + 1)]

        for _ in range(self.R - 1):
            for u, v, w in self.edges:
                if dist[u] != INFTY and dist[u] + w < dist[v]:
                    dist[v] = dist[u] + w

        for u, v, w in self.edges:
            if dist[u] != INFTY and dist[u] + w < dist[v]:
                # Contains negative weight cycle which corresponds to infinite trading
                return True

        return False


def entrypoint(stream: TextIO) -> Generator[bool, None, None]:
    while True:
        (T, R, useless, needed) = map(int, stream.readline().strip().split())

        if (T, R, useless, needed) == (0, 0, 0, 0):
            # Terminating condition
            return

        g = Graph(T, R, stream)

        yield g.inverse_bellman_ford(useless)  # and g.path_exists(useless, needed)


if __name__ == "__main__":
    for result in entrypoint(stdin):
        print(str(result).upper())