from typing import List


def print2d(arr: List[List[int]]):
    for i in arr:
        for j in i:
            print(j, end=" ")
        print()


def skynet():
    X = [0, 4, 7, 6, 3]
    Y = [0, 1, 2, 4, 8]
    n = 5

    DP = [[0 for _ in range(0, i + 1)] for i in range(0, n)]
    print2d(DP)

    for i in range(1, n):
        prev = 0
        for k in range(1, i + 1):
            DP[i][k] = DP[i - 1][k - 1]
            val = DP[i - 1][k - 1] + min(X[i], Y[k])
            if val > prev:
                prev = val
        DP[i][0] = prev
#for j in range(1, i):
#            DP[i][j] = DP[i - 1][j - 1]
        print()
        print2d(DP)


if __name__ == "__main__":
    skynet()
