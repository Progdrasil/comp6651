from collections import defaultdict
from sys import stdin
from typing import Dict, Generator, List, TextIO
from math import gcd as mgcd

GCD_MEM: Dict[int, Dict[int, int]] = defaultdict(dict)
D: Dict[str, int] = {}


def gcd(first: int, second: int) -> int:
    val = None
    if GCD_MEM[first].get(second) is None:
        val = mgcd(first, second)
        GCD_MEM[first][second] = val
        GCD_MEM[second][first] = val

    return val or GCD_MEM[first][second]


def rest(values: List[int], i: int) -> List[int]:
    return values[:i] + values[i + 1 :]


def min_gcd(values: List[int], n: int) -> int:
    D: List[List[int]] = [
        [gcd(values[i], values[j]) if i != j else 0 for i in range(n)] for j in range(n)
    ]
    # gcd(values[i], values[j]) if i != j else

    for j in range(n):
        for i in range(j - 2, -1, -1):
            pot: List[int] = [
                gcd(values[i + 1], values[j]) + D[i + 1][j],
                gcd(values[i], values[j - 1]) + D[i][j - 1],
                gcd(values[i], values[j]) * 2 + D[i + 1][j - 1],
            ]

            for k in range(i + 1, j):
                pot.append(D[i][k] + D[k][j] + gcd(values[k - 1], values[k + 1]))

            D[i][j] = min(pot)

    print2(D)
    return D[0][n - 1]


def print2(D: List[List[int]]):
    print("  |", end="")
    for i in range(len(D)):
        if i < 10:
            print(i, end=" |")
        else:
            print(i, end="|")
    print()

    for j in range(len(D)):
        if j < 10:
            print(j, end=" |")
        else:
            print(j, end="|")
        for i in range(j + 1):
            if D[i][j] < 10:
                print(D[i][j], end=" |")
            else:
                print(D[i][j], end="|")
        for _ in range(j + 1, len(D)):
            print("  ", end="|")
        print()


def entrypoint(stream: TextIO) -> Generator[int, None, None]:
    line = stream.readline().strip().split()
    n = int(line[0])
    while n > 0:
        values = [int(i) for i in line[1:]]

        yield min_gcd(values, n)

        line = stream.readline().strip().split()
        n = int(line[0])

    return


if __name__ == "__main__":
    for out in entrypoint(stdin):
        print(out)
