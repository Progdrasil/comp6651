import java.util.Scanner;

public class Game {

    public static int gcd(int x, int y) {
	if (x == 0)
	    return y;
	if (y == 0)
	    return x;
	if (x > y)
	    return gcd(y, x % y);
	return gcd(x, y % x);
    }

    public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	int n = input.nextInt();
	int c = 1;
	while (n > 0) {
	    int[] A = new int[n];
	    for (int i = 0; i < n; i++) {
		A[i] = input.nextInt();
	    }
	    int[][] M = new int[n][n];
	    for (int i = 0; i < n; i++) {
		M[i][i] = gcd(A[(i - 1 + n) % n], A[(i + 1) % n]);
	    }
	    for (int l = 1; l < n - 2; l++) {
		for (int i = 0; i < n; i++) {
		    int j = (i + l) % n;
		    if (i < j) {
			int best = Math.min(M[i][j - 1], M[i + 1][j]);
			for (int k = i + 1; k < j; k++) {
			    best = Math.min(best, M[i][k - 1] + M[k + 1][j]);
			}
			M[i][j] = best + gcd(A[(i - 1 + n) % n], A[(j + 1) % n]);
		    } else { // i > j
			int best = Math.min(M[i][(j - 1 + n) % n], M[(i + 1) % n][j]);
			for (int k = i + 1; k < n; k++) {
			    best = Math.min(best, M[i][k - 1] + M[(k + 1) % n][j]);
			}
			for (int k = 0; k < j; k++) {
			    best = Math.min(best, M[i][(k - 1 + n) % n] + M[(k + 1) % n][j]);
			}
			M[i][j] = best + gcd(A[(i - 1 + n) % n], A[(j + 1) % n]);
		    }
		}
	    }
	    int best = M[1][n - 2] + gcd(A[0], A[n - 1]);
	    for (int i = 0; i < n - 1; i++) {
		best = Math.min(best, gcd(A[i], A[i + 1]) + M[(i + 2) % n][(i - 1 + n) % n]);
	    }
	    for (int i = 0; i < n - 1; i++) {
		for (int l = 2; l < n - 1; l++) {
		    int j = (i + l) % n;
		    best = Math.min(best,
			    gcd(A[i], A[j]) + M[(i + 1) % n][(j - 1 + n) % n] + M[(j + 1) % n][(i - 1 + n) % n]);
		}
	    }
	    System.out.println(best);
	    c++;
	    n = input.nextInt();
	}
	input.close();
    }

}
