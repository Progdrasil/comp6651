#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int counts[32][1024];

int main() {
    int totc;
    cin >> totc;
    
    //cerr << "totc " << totc << endl;
    
    for(int zzz = 0; zzz < totc; zzz++) {
        int v, d;
        cin >> v >> d;
        vector<int> prices(v, 0);
        for(int i = 0; i < v; i++) {
            cin >> prices[i];
        }
        sort(prices.begin(), prices.end());
        int res = 0;
        for(int k = 0; k <= v; k++) {
            /* buy the first k items, do not buy (k+1)st item */
            int budget = d;
            for(int i = 0; i < k; i++) {
                budget -= prices[i];
            }
            if(budget < 0) break;
            for(int i = 0; i <= budget; i++){
                counts[0][i] = 0;
            }
            counts[0][0] = 1;
            for(int i = k+1; i < v; i++) {
                for(int money = 0; money <= budget; money++) {
                    counts[i-k][money] = counts[i-k-1][money];
                    if(money >= prices[i]) {
                        counts[i-k][money] += counts[i-k-1][money-prices[i]];
                    }
                }
            }
            for(int j = budget; j >= max(0, budget-prices[k]+1); j--){
                res += counts[v-k-1][j];
            }
        }
        if(d < prices[0]) res = 0;
        cout << (zzz+1) << " " << res << endl;
    }
    return 0;
    
}
