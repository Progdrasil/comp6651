from typing import List, TextIO
from sys import stdin


def entrypoint(stream: TextIO) -> int:
    (n, budget) = map(int, stream.readline().strip().split())
    gifts: List[int] = []
    while len(gifts) != n:
        gifts.extend(map(int, stream.readline().strip().split()))

    return count_weighted_groups(gifts, budget)


def count_weighted_groups(P: List[int], budget: int) -> int:
    D = [[0 for _ in range(0, budget + 1)] for _ in range(0, len(P) + 1)]

    P.sort()
    P.insert(0, 0)

    for i in range(1, len(P)):
        for j in range(0, budget + 1):
            D[i][j] = D[i - 1][j]
            if j >= P[i]:
                if D[i - 1][j - P[i]] == 0:
                    D[i][j] += 1
                elif j - sum(P[: i + 1]) >= 0:
                    pass
                else:
                    D[i][j] += D[i - 1][j - P[i]]
    return D[len(P) - 1][budget]


if __name__ == "__main__":
    cases = int(stdin.readline().strip())
    for i in range(1, cases + 1):
        print(f"{i} {entrypoint(stdin)}")
