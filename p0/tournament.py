import sys
from typing import TextIO, List


def get_students(num: int, stream: TextIO) -> List[int]:
    return [int(stream.readline().strip()) for _ in range(num)]


def calc_rating(students: List[int]) -> int:
    half = int(len(students) / 2)
    left = students[:half]
    right = students[half:]
    sums = [left[i] + right[half - i - 1] for i in range(half)]

    return min(sums)


def algorithm(num: int, stream: TextIO) -> int:
    s = get_students(num, stream)
    s.sort()
    return calc_rating(s)


if __name__ == "__main__":
    n = int(input())
    x = algorithm(n, sys.stdin)
    print(x)
