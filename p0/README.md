# Huge programming tournament

**Due Date:** 24-01-2021 23:55
**Timelimit:** 2 seconds

## Description
You are setting up a huge programming tournament of teams consisting of pairs of students.
You are in charge of putting teams together.
Every student must be placed on exactly one team of two students.
Fortunately, there are even number of students who wish to compete.
You need to make sure that each team meets some minimum total rating.
The total rating of a team is defined as the sum of the ratings of both students on the team.
Write a program to determine the maximum value $`X`$ such that you can form teams, each of which has total rating greater than or equal to $`X`$.

## Input

The first line of input contains a single positive integer $`n`$ ($`1 ≤ n ≤ 10^5`$, $`n`$ is even), the number of students who want to enter the competition.
Each of the following $`n`$ lines contains one single integer $`s_i( 1 ≤ s_i ≤ 106 )`$, the rating of student $`i`$.

## Output

Print, on a single line, the maximum value $`X`$ such that you can form teams where every team has a total rating greater than or equal to $`X`$.

### Examples

**input**
```
4
1
2
3
5
```
**output**
```
5
```

**input**
```
2
18
16
```
**output**
```
34
```