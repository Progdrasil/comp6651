from io import StringIO
from typing import Generator, Optional, TextIO
from sys import stdin
from math import floor, log10


def next_entry(previous: str, next: str) -> str:
    return previous + next[:1]


class Dictionary:
    def __init__(self, line: str):
        split_line = line.split()
        n = int(split_line[0])
        self.dict = [split_line[i] for i in range(1, n + 1)]
        self.entries = {s for s in self.dict}
        self.previous: str = ""

    def coding_size(self) -> int:
        return floor(log10(len(self.dict))) + 1

    def add_to_dict(self, entry: str):
        if entry not in self.entries:
            self.dict.append(entry)
            self.entries.add(entry)

    def get_value(self, index: int) -> str:
        if index < len(self.dict):
            val = self.dict[index]
            self.add_to_dict(next_entry(self.previous, val))
        else:
            val = next_entry(self.previous, self.previous)
            self.add_to_dict(val)

        self.previous = val

        return val

    def read_next_key(self, encoded: StringIO) -> str:
        size = self.coding_size()
        code = encoded.read(size)
        index = int(code)
        return self.get_value(index)

    def decode(self, encoded: str) -> str:
        encoded_len = len(encoded)
        encoded_stream = StringIO(encoded)
        decoded = ""
        while encoded_stream.tell() < encoded_len:
            decoded += self.read_next_key(encoded_stream)

        return decoded


def entrypoint(stream: TextIO) -> Generator[str, None, None]:
    encoded = stream.readline().strip()
    while len(encoded) >= 1 and int(encoded) != 0:
        dictionary = Dictionary(stream.readline().strip())
        yield dictionary.decode(encoded)
        encoded = stream.readline().strip()


if __name__ == "__main__":
    for i, s in enumerate(entrypoint(stdin)):
        print(f"Case {i+1}: {s}")
