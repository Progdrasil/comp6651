from typing import List


def print2d(l: List[List[int]]):
    for r in l:
        for c in r:
            print(c, end=" ")
        print()
