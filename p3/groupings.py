from typing import List, TextIO, Optional
from sys import stdin


def entrypoint(stream: TextIO) -> Optional[int]:
    line = stream.readline().strip()
    if line == "bye":
        return None

    digits = [int(c) for c in line]
    n = len(digits)
    D = [[-1 for _ in range(0, n)] for _ in range(0, n)]
    result = count_groupings(digits, D, 0, 0, 0, n)
    # result = countGroups(0, 0, n, digits)
    # print2d(D)
    return result


def count_groupings(
    digits: List[int],
    D: List[List[int]],
    lhs_sum: int,
    lhs_group_size: int,
    position: int,
    length: int,
) -> int:
    if position == length:
        return 1

    if D[position][lhs_group_size] != -1:
        return D[position][lhs_group_size]
    # print2d(D)
    # print()

    D[position][lhs_group_size] = 0
    # res = 0
    rhs_sum = 0
    group_size = 0

    for i in range(position, length):
        rhs_sum += digits[i]
        group_size += 1
        if rhs_sum >= lhs_sum:
            D[position][lhs_group_size] += count_groupings(
                digits, D, rhs_sum, group_size, i + 1, length
            )

    # D[position][lhs_sum] = res

    return D[position][lhs_group_size]


if __name__ == "__main__":
    counter = 1
    res = entrypoint(stdin)
    while res is not None:
        print(f"{counter}. {res}")
        counter += 1
        res = entrypoint(stdin)