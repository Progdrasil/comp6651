# Groupings

**Due Date:** 2021-02-14 23:55
**TimeLimit:** 2 sec (C/C++), 4 sec (Java), 12 sec (python)

## Description
You are given a non-empty string consisting of digits. You may group digits together (in the original order) if for every group, except for the last one, the sum of the digits in the group is less than or equal to the sum of the digits in the following group (immediately to the right). Each digit must belong to at least one group. Write a program to compute the number of possible groupings for a given string of digits.

### Example 1

Suppose that the string is 9876. Then there are two possible groupings:
1. the grouping consisting of a single group (9876);
2. the grouping consisting of two groups (9)(876), since $`9 \leq 8 + 7 + 6 = 21`$. Notice that the grouping (98)(76) is not allowed, since $`9 + 8 > 7 + 6`$.

Therefore, the answer for the input 9876 is 2.

### Example 2
There are 7 possible groupings for string 1117: (1117), (111)(7), (11)(17),(1)(117), (1)(11)(7), (1)(1)(17), (1)(1)(1)(7).

## Input
There are several test cases in a single input. Each test case is specified on a separate line. Each line consists of a string of at most 25 decimal digits. The end of input is specified by a line containing the word “bye”, which is not part of the test cases.

## Output
For each test case, write the result using the format: `k. n` where $`k`$ is the test case number (starting with 1) and $`n`$ is the answer for that test case.

### Sample
**input:**
```
635
1117
9876
bye
```

**output:**
```
1. 2
2. 7
3. 2
```