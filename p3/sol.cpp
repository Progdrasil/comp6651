#include <iostream>
using namespace std;

int main() {
  int caseno = 1;

  while (true) {
    string line;
    getline(cin, line);
    if(line[0] == 'b')
      break;
    int count[30][250];
    for(int i = 0; i < 250; i++) {
      count[0][i] = 1;
    }
    line.erase(line.find_last_not_of(" \n\r\t")+1);
    int n = line.size();
    //    cout << " LINE SIZE: " << n << endl;
    for(int i = 1; i <= n; i++) {
      for(int s = 0; s< 250; s++) {
	count[i][s] = 0;
	int cs = 0;
	for(int k = 1; k <= i; k++) {
	  int curno = line[i-k]-'0';
	  cs = curno + cs;
	  if(cs <= s) {
	    count[i][s] += count[i-k][cs];
	  }
	}
      }
    }
    /*    int res = 0;
    for(int s = 0; s < 250; s++) {
      if(count[n][s] > 0) {
	cout << "sum " << s << "is achievable " << endl;
      }
      res += count[n][s];
      }*/
    cout << caseno << ". " << count[n][249] << endl;
    caseno++;
  }
  
  return 0;
}
